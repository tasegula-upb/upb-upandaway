#pragma once
#include "framework\lab_mesh_loader.hpp"
#include "framework\lab_geometry.hpp"
#include "framework\lab_shader_loader.hpp"
#include "framework\lab_glut.hpp"
#include "framework\lab_texture_loader.hpp"
#include "framework\lab_camera.hpp"
#include "framework\lab_framebuffer.hpp"

#include "sources\Map.h"

#include "util\resources.h"
#include "util\shaders.h"
#include "util\camera.h"

// system internal headers
#include "windows.h"
#include "mmsystem.h"
#include <ctime>

class Laborator : public lab::glut::WindowListener {

	//variabile
private:
	lab::Camera camera;
	const glm::mat4 uniform_matrix = glm::mat4(1);
	glm::mat4 view_matrix, projection_matrix, model_matrix;
	unsigned int heightTexture;
	unsigned int screen_width, screen_height;
	unsigned int manual_tes;

	lab::Mesh mesh_cubemap;
	unsigned int texture_cubemap;

	//factori teselare
	unsigned int tess_factor_inner, tess_factor_outer;
	unsigned quad_texture_color, quad_texture_displacement;
	float max_displacement;
	unsigned int shader_cubemap;

	Map *map;
	//metode
public:

#pragma region CONSTRUCTOR
	Laborator() {
		//setari pentru desenare, clear color seteaza culoarea de clear pentru ecran (format R,G,B,A)
		glClearColor(0.5, 0.5, 0.5, 1);
		glClearDepth(1);			//clear depth si depth test (nu le studiem momentan, dar avem nevoie de ele!)
		glEnable(GL_DEPTH_TEST);	//sunt folosite pentru a determina obiectele cele mai apropiate de camera (la curs: algoritmul pictorului, algoritmul zbuffer)

		//incarca un shader din fisiere si gaseste locatiile matricilor relativ la programul creat
		map = new Map(resource::map_file);
		map->shader = lab::loadShader(shader::map_vertex, shader::map_tcp, shader::map_tes, shader::map_fragment);
		shader_cubemap = lab::loadShader("..\\shaders\\normal_vertex.glsl", "..\\shaders\\normal_fragment.glsl");
		map->texture = lab::loadTextureBMP(resource::map_tex);
		quad_texture_displacement = lab::loadTextureBMP("..\\resources\\heightMap2.bmp");

		lab::loadObj("..\\resources\\box.obj", mesh_cubemap);
		texture_cubemap = lab::loadTextureCubemapBMP("..\\resources\\xpos.bmp", "..\\resources\\yneg.bmp", "..\\resources\\zpos.bmp", "..\\resources\\xneg.bmp", "..\\resources\\ypos.bmp", "..\\resources\\zneg.bmp");

		//factori de teselare
		manual_tes = 0;
		tess_factor_inner = 2;
		tess_factor_outer = 2;
		max_displacement = 5;

		model_matrix = glm::mat4(1);
		model_matrix = glm::translate(model_matrix, glm::vec3(2500, 0, 2500));
		model_matrix = glm::scale(model_matrix, glm::vec3(map->height * map->scale / 512));
		model_matrix = glm::rotate(model_matrix, 180.0f, glm::vec3(0, 0, 1.0f));

		//matrici de modelare si vizualizare
		camera.set(glm::vec3(2500, 30, 2700), glm::vec3(2500, 0, 2500), glm::vec3(0, 1, 0));
		initCameraKeys();

		PlaySound(resource::audio.c_str(), NULL, SND_LOOP | SND_ASYNC);

		glPatchParameteri(GL_PATCH_VERTICES, 3);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
#pragma endregion

#pragma region DESTRUCTOR
	~Laborator() {
		//distruge shader
		glDeleteProgram(map->shader);
		glDeleteProgram(shader_cubemap);
	}
#pragma endregion

	//--------------------------------------------------------------------------------------------
	//functii de cadru ---------------------------------------------------------------------------
	void notifyBeginFrame() {
	}
	//functia de afisare (lucram cu banda grafica)
	void notifyDisplayFrame() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, screen_width, screen_height);
		moveCamera();
		drawCubeMap();
		drawMap();
	}
	//functie chemata dupa ce am terminat cadrul de desenare (poate fi folosita pt modelare/simulare)
	void notifyEndFrame() {
	}
	//functei care e chemata cand se schimba dimensiunea ferestrei initiale
	void notifyReshape(int width, int height, int previos_width, int previous_height) {
		//reshape
		if(height == 0) height = 1;
		float aspect = (float) width / (float) height;
		screen_width = width;
		screen_height = height;
		projection_matrix = glm::perspective(75.0f, aspect, 0.1f, 10000.0f);
	}

#pragma region INPUT METHODS
	//--------------------------------------------------------------------------------------------
	//functii de input output --------------------------------------------------------------------
	//tasta apasata
	void notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y) {
		static bool wire = false;

		switch(key_pressed) {
		case KEY_ESC:
			// ESC - close glut
			lab::glut::close();
			break;
		case(KEY_SPACE) :
			//SPACE - reinit shader
			glDeleteProgram(map->shader);
			glDeleteProgram(shader_cubemap);
			map->shader = lab::loadShader(shader::map_vertex, shader::map_tcp, shader::map_tes, shader::map_fragment);
			shader_cubemap = lab::loadShader("..\\shaders\\normal_vertex.glsl", "..\\shaders\\normal_fragment.glsl");

		case('t') :
		case('T') :
							wire = !wire;
			glPolygonMode(GL_FRONT_AND_BACK, (wire ? GL_LINE : GL_FILL));
			break;

		case('c') :
		case('C') :
							map->color_type = !map->color_type;
			break;

			// --------------------------------------------------------------------------
			// CAMERA CONTROLS
			// --------------------------------------------------------------------------
		case 'w': case 'W': keys[CAM_TRANS_X_POS] = true; break;
		case 's': case 'S': keys[CAM_TRANS_X_NEG] = true; break;
		case 'd': case 'D': keys[CAM_TRANS_Y_POS] = true; break;
		case 'a': case 'A': keys[CAM_TRANS_Y_NEG] = true; break;
		case 'r': case 'R': keys[CAM_TRANS_Z_POS] = true; break;
		case 'f': case 'F': keys[CAM_TRANS_Z_NEG] = true; break;
		case '8': keys[CAM_ROTATE_X_NEG] = true; break;
		case '2': keys[CAM_ROTATE_X_POS] = true; break;
		case '6': keys[CAM_ROTATE_Y_POS] = true; break;
		case '4': keys[CAM_ROTATE_Y_NEG] = true; break;
		case '3': keys[CAM_ROTATE_Z_NEG] = true; break;
		case '1': keys[CAM_ROTATE_Z_POS] = true; break;
		default:
			if(key_pressed == '-') {
				if(tess_factor_inner > 2) tess_factor_inner /= 2;
				std::cout << "Tess Factor Inner divided:  " << tess_factor_inner << std::endl;
			}
			if(key_pressed == '=') {
				if(tess_factor_inner <= 64) tess_factor_inner *= 2;
				std::cout << "Tess Factor Inner multiplied:  " << tess_factor_inner << std::endl;
			}
			if(key_pressed == '9') {
				if(tess_factor_outer > 2) tess_factor_outer /= 2;
				std::cout << "Tess Factor Outer divided:  " << tess_factor_outer << std::endl;
			}
			if(key_pressed == '0') {
				if(tess_factor_outer <= 64) tess_factor_outer *= 2;
				std::cout << "Tess Factor Outer multiplied:  " << tess_factor_outer << std::endl;
			}
			if(key_pressed == 'm' || key_pressed == 'M') {
				manual_tes = !manual_tes;
				std::cout << "Manual Tesselation:  " << manual_tes << std::endl;
			}
			break;
		}
	}
	//tasta ridicata
	void notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y) {
		switch(key_released) {
		case 'w': case 'W': keys[CAM_TRANS_X_POS] = false; break;
		case 's': case 'S': keys[CAM_TRANS_X_NEG] = false; break;
		case 'd': case 'D': keys[CAM_TRANS_Y_POS] = false; break;
		case 'a': case 'A': keys[CAM_TRANS_Y_NEG] = false; break;
		case 'r': case 'R': keys[CAM_TRANS_Z_POS] = false; break;
		case 'f': case 'F': keys[CAM_TRANS_Z_NEG] = false; break;
		case '8': keys[CAM_ROTATE_X_NEG] = false; break;
		case '2': keys[CAM_ROTATE_X_POS] = false; break;
		case '6': keys[CAM_ROTATE_Y_POS] = false; break;
		case '4': keys[CAM_ROTATE_Y_NEG] = false; break;
		case '3': keys[CAM_ROTATE_Z_NEG] = false; break;
		case '1': keys[CAM_ROTATE_Z_POS] = false; break;
		}
	}
	//tasta speciala (up/down/F1/F2..) apasata
	void notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y) {
		if(key_pressed == GLUT_KEY_F1) lab::glut::enterFullscreen();
		if(key_pressed == GLUT_KEY_F2) lab::glut::exitFullscreen();

	}
	//tasta speciala ridicata
	void notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y) {
	}
	//drag cu mouse-ul
	void notifyMouseDrag(int mouse_x, int mouse_y) {
	}
	//am miscat mouseul (fara sa apas vreun buton)
	void notifyMouseMove(int mouse_x, int mouse_y) {
	}
	//am apasat pe un boton
	void notifyMouseClick(int button, int state, int mouse_x, int mouse_y) {
	}
	//scroll cu mouse-ul
	void notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y) {
	}
#pragma endregion

	// ---------------------------------------------------------------------------
	// HELPERS
	// ---------------------------------------------------------------------------

	void drawCubeMap() {
		glUseProgram(shader_cubemap);
		glUniformMatrix4fv(glGetUniformLocation(shader_cubemap, "view_matrix"), 1, false, glm::value_ptr(camera.getViewMatrix()));
		glUniformMatrix4fv(glGetUniformLocation(shader_cubemap, "projection_matrix"), 1, false, glm::value_ptr(projection_matrix));
		glUniformMatrix4fv(glGetUniformLocation(shader_cubemap, "model_matrix"), 1, false, glm::value_ptr(model_matrix));
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, texture_cubemap);
		glUniform1i(glGetUniformLocation(shader_cubemap, "textura_cubemap"), 1);
		mesh_cubemap.Bind();
		mesh_cubemap.DrawNormal();
	}

	void drawMap() {
		glUseProgram(map->shader);

		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, map->texture);
		glUniform1i(glGetUniformLocation(map->shader, "textura_color"), 1);
		glActiveTexture(GL_TEXTURE0 + 6);
		glBindTexture(GL_TEXTURE_2D, quad_texture_displacement);
		glUniform1i(glGetUniformLocation(map->shader, "map_displacement"), 6);

		glUniform1i(glGetUniformLocation(map->shader, "use_texture"), map->color_type);

		glUniformMatrix4fv(glGetUniformLocation(map->shader, "view_matrix"), 1, false, glm::value_ptr(camera.getViewMatrix()));
		glUniformMatrix4fv(glGetUniformLocation(map->shader, "projection_matrix"), 1, false, glm::value_ptr(projection_matrix));
		glUniformMatrix4fv(glGetUniformLocation(map->shader, "model_matrix"), 1, false, glm::value_ptr(uniform_matrix));
		glUniform1i(glGetUniformLocation(map->shader, "tess_factor_inner"), tess_factor_inner);
		glUniform1i(glGetUniformLocation(map->shader, "tess_factor_outer"), tess_factor_outer);
		glUniform1i(glGetUniformLocation(map->shader, "manual_tes"), manual_tes);
		glUniform3fv(glGetUniformLocation(map->shader, "light_position"), 1, glm::value_ptr(map->light_position));
		glUniform3fv(glGetUniformLocation(map->shader, "eye_position"), 1, glm::value_ptr(camera.getPosition()));
		glUniform1f(glGetUniformLocation(map->shader, "max_displacement"), max_displacement);
		glUniform1i(glGetUniformLocation(map->shader, "material_shininess"), map->material_shininess);
		glUniform1f(glGetUniformLocation(map->shader, "material_kd"), map->material_kd);
		glUniform1f(glGetUniformLocation(map->shader, "material_ks"), map->material_ks);

		map->mesh.Bind();
		map->mesh.Draw();
	}


	void moveCamera() {
		static int degree = 5;

		if(keys[CAM_TRANS_X_POS]) {			// W
			camera.translateForward(+DIST);
		}
		if(keys[CAM_TRANS_X_NEG]) {			// S
			camera.translateForward(-DIST);
		}
		if(keys[CAM_TRANS_Y_POS]) {			// D
			camera.translateRight(+DIST);
		}
		if(keys[CAM_TRANS_Y_NEG]) {			// A
			camera.translateRight(-DIST);
		}
		if(keys[CAM_TRANS_Z_POS]) {			// R
			camera.translateUpword(+DIST);
		}
		if(keys[CAM_TRANS_Z_NEG]) {			// F
			camera.translateUpword(-DIST);
		}
		if(keys[CAM_ROTATE_X_NEG]) {			// 8
			camera.rotateFPSoX(-PI / degree);
		}
		if(keys[CAM_ROTATE_X_POS]) {			// 2
			camera.rotateFPSoX(+PI / degree);
		}
		if(keys[CAM_ROTATE_Y_NEG]) {			// 4
			camera.rotateFPSoY(-PI / degree);
		}
		if(keys[CAM_ROTATE_Y_POS]) {			// 6
			camera.rotateFPSoY(+PI / degree);
		}
		if(keys[CAM_ROTATE_Z_NEG]) {			// 3
			camera.rotateFPSoZ(-PI / degree);
		}
		if(keys[CAM_ROTATE_Z_POS]) {			// 1
			camera.rotateFPSoZ(+PI / degree);
		}
	}

};