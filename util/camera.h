// ----------------------------------------------------------------------------
// KEYS
// ----------------------------------------------------------------------------

#define KEY_ESC 27
#define KEY_SPACE 32

#define KEY_CAM_TRANS_X_POS 'd'
#define KEY_CAM_TRANS_X_NEG 'a'
#define KEY_CAM_TRANS_Y_POS 'r'
#define KEY_CAM_TRANS_Y_NEG 'f'
#define KEY_CAM_TRANS_Z_POS 'w'
#define KEY_CAM_TRANS_Z_NEG 's'

#define KEY_CAM_ROTATE_X_NEG '8'
#define KEY_CAM_ROTATE_X_POS '2'
#define KEY_CAM_ROTATE_Y_NEG '6'
#define KEY_CAM_ROTATE_Y_POS '4'
#define KEY_CAM_ROTATE_Z_NEG '3'
#define KEY_CAM_ROTATE_Z_POS '1'

#define CAM_TRANS_Z_POS 5
#define CAM_TRANS_Z_NEG 6
#define CAM_TRANS_X_POS 7
#define CAM_TRANS_X_NEG 8
#define CAM_TRANS_Y_POS 9
#define CAM_TRANS_Y_NEG 10

#define CAM_ROTATE_X_NEG 11
#define CAM_ROTATE_X_POS 12
#define CAM_ROTATE_Y_NEG 13
#define CAM_ROTATE_Y_POS 14
#define CAM_ROTATE_Z_NEG 15
#define CAM_ROTATE_Z_POS 16

bool keys[20];

void initCameraKeys() {
	for(int i = 0; i < 20; i++) {
		keys[i] = false;
	}
}

// ----------------------------------------------------------------------------
// TECHNICAL
// ----------------------------------------------------------------------------

#define UP		0
#define DOWN	1
#define RIGHT	2
#define LEFT	3

#define PI 3.14159265358
#define DIST 10
