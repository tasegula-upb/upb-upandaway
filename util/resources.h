#pragma once
#include <string>

namespace resource {
	const std::string &folder = "..\\resources\\";

	const std::string &map_tex = folder + "ground.bmp";
	const std::string &map_file = folder + "heightmap.pgm";

	const std::string &audio = folder + "forest.wav";
}