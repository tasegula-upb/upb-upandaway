#pragma once
#include <string>

namespace shader {
	const std::string &folder = "..\\shaders\\";

	//*
	const std::string &map_vertex = folder + "shader_map_vertex.glsl";
	const std::string &map_geometry = folder + "shader_map_geometry.glsl";
	const std::string &map_fragment = folder + "shader_map_fragment.glsl";
	const std::string &map_tcp = folder + "shader_map_tcp.glsl";
	const std::string &map_tes = folder + "shader_map_tes.glsl";
	//*/

	/*
	const std::string &map_vertex = folder + "shader_vertex.glsl";
	const std::string &map_geometry = folder + "shader_geometry.glsl";
	const std::string &map_fragment = folder + "shader_fragment.glsl";
	const std::string &map_tcp = folder + "shader_tess_control.glsl";
	const std::string &map_tes = folder + "shader_tess_eval.glsl";
	//*/
}