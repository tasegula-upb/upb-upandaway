#include "..\framework\lab_mesh_loader.hpp"

#define USE_COLOR 0
#define USE_TEXTURE 1

typedef unsigned char UCHAR;

class Map {
private:
	unsigned char **pixel;

#pragma region HELPERS
	void initPixel();
	bool readImage(std::string filename);
	void generateMesh();
	void generateQuad();
#pragma endregion
public:
	// OpenGL stuff
	lab::Mesh mesh;
	unsigned int texture;
	unsigned int shader;

	unsigned int width, height, scale;
	unsigned int color_type;

	glm::vec3 light_position = glm::vec3(1000, 500, 1000);
	unsigned int material_shininess = 20;
	float material_kd = 0.9;
	float material_ks = 0.3;

#pragma region CONSTRUCTORS
	Map(const std::string filename);
	Map(const std::string filename, int _scale);
#pragma endregion

#pragma region API
	void destroyGL();
#pragma endregion
};