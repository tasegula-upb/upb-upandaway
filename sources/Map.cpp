#include "Map.h"

Map::Map(std::string filename, int _scale) {
	scale = (_scale > 0) ? _scale : 10;
	color_type = USE_COLOR;
	bool result = readImage(filename);
	if(result) {
		generateMesh();
	}
}

Map::Map(std::string filename) {
	scale = 10;
	color_type = USE_COLOR;
	bool result = readImage(filename);
	if(result) {
		generateMesh();
	}
}

bool Map::readImage(std::string filename) {
	const int maxline = 256;
	char line[maxline];
	std::filebuf fb;

	if(!fb.open(filename, std::ios::in | std::ios::binary)) {
		std::cout << "Error opening heightmap file!" << std::endl;
		return false;
	}

	std::istream istr(&fb);

	//read file format
	istr.getline(line, maxline);

	//read file info
	istr.getline(line, maxline);

	//read map width
	istr >> width;
	istr >> height;

	//read max value
	int a;
	istr >> a;

	initPixel();

	std::ofstream out("_debug.txt");
	if(!out.is_open()) {
		std::cout << "oooops";
	}

	for(int i = 0; i < height; i++) {
		for(int j = 0; j < width; j++) {
			istr >> a;
			pixel[i][j] = static_cast<unsigned char>(a);

			out << pixel[i][j] << " ";
		}
		out << "\n";
	}

	fb.close();
	out.flush();
	out.close();
	return true;
}

void Map::generateMesh() {
	std::vector<lab::VertexFormat> vertices;
	std::vector<unsigned int> indices;
	int index = 0;

	//glm::vec2 LEFT_TOP = glm::vec2(0, 0);
	//glm::vec2 RIGHT_TOP = glm::vec2(1, 0);
	//glm::vec2 RIGHT_BOTTOM = glm::vec2(1, 1);
	//glm::vec2 LEFT_BOTTOM = glm::vec2(0, 1);

	//float dist;
	//float accH = 1.0f / (height - 1.0f);
	//float accW = 1.0f / (width - 1.0f);

	//for (int i = 0; i < height - 1; i++) {
	//	for (int j = 0; j < width - 1; j++) {

	//		glm::vec3 p1 = glm::vec3(j * scale, pixel[i][j], i * scale);
	//		glm::vec3 p2 = glm::vec3((j + 1) * scale, pixel[i][j + 1], i * scale);
	//		glm::vec3 p3 = glm::vec3((j + 1) * scale, pixel[i + 1][j + 1], (i + 1) * scale);
	//		glm::vec3 p4 = glm::vec3(j * scale, pixel[i + 1][j], (i + 1) * scale);

	//		glm::vec3 n1 = glm::cross(glm::normalize(p2 - p1), glm::normalize(p1 - p4));
	//		glm::vec3 n2 = glm::cross(glm::normalize(p2 - p3), glm::normalize(p1 - p2));
	//		glm::vec3 n3 = glm::cross(glm::normalize(p3 - p4), glm::normalize(p2 - p3));
	//		glm::vec3 n4 = glm::cross(glm::normalize(p4 - p1), glm::normalize(p3 - p4));

	//		glm::vec2 t1 = glm::vec2(i * accH, j * accW);
	//		glm::vec2 t2 = glm::vec2(i * accH, (j + 1) * accW);
	//		glm::vec2 t3 = glm::vec2((i + 1) * accH, (j + 1) * accW);
	//		glm::vec2 t4 = glm::vec2((i + 1) * accH, j * accW);

	//		dist = std::abs(p1.y - p2.y);

	//		//*
	//		vertices.push_back(lab::VertexFormat(p1, n1, LEFT_TOP));
	//		vertices.push_back(lab::VertexFormat(p2, n2, RIGHT_TOP));
	//		vertices.push_back(lab::VertexFormat(p3, n3, RIGHT_BOTTOM));
	//		vertices.push_back(lab::VertexFormat(p4, n4, LEFT_BOTTOM));
	//		//*/

	//			indices.push_back(index); indices.push_back(index + 1); indices.push_back(index + 2);
	//			indices.push_back(index + 2); indices.push_back(index + 3); indices.push_back(index);
	//			index += 4;
	//	}
	//}
	//

	for(unsigned int i = 0; i < height; ++i) {
		for(unsigned int j = 0; j < width; ++j) {
			lab::VertexFormat v;

			v.position_x = j * scale;
			v.position_y = pixel[i][j] + 1;
			v.position_z = i * scale;

			v.normal_x = 0.0f;
			v.normal_y = 1.0f;
			v.normal_x = 0.0f;
			v.texcoord_x = (float) j / (float) (width - 1);
			v.texcoord_y = (float) i / (float) (height - 1);

			vertices.push_back(v);
		}
	}

	for(int i = 0; i < height - 1; i++) {
		for(int j = 0; j < width - 1; j++) {
			int id = i * width + j;
			glm::vec3 p1 = vertices.at(id).position();
			glm::vec3 p2 = vertices.at(id + 1).position();
			glm::vec3 p3 = vertices.at(id + width + 1).position();
			glm::vec3 p4 = vertices.at(id + width).position();


			glm::vec3 n1 = glm::cross(glm::normalize(p2 - p1), glm::normalize(p1 - p4));
			glm::vec3 n2 = glm::cross(glm::normalize(p2 - p3), glm::normalize(p1 - p2));
			glm::vec3 n3 = glm::cross(glm::normalize(p3 - p4), glm::normalize(p2 - p3));
			glm::vec3 n4 = glm::cross(glm::normalize(p4 - p1), glm::normalize(p3 - p4));

			vertices[id].setNormal(n1);
			vertices[id + 1].setNormal(n2);
			vertices[id + width + 1].setNormal(n3);
			vertices[id + width].setNormal(n4);



		}


	}

	for(int i = 0; i < height - 1; ++i) {
		for(int j = 0; j < width - 1; ++j) {
			int id = i * width + j;
			indices.push_back(id);
			indices.push_back(id + width);
			indices.push_back(id + width + 1);
			indices.push_back(id + width + 1);
			indices.push_back(id + 1);
			indices.push_back(id);
		}
	}

	lab::createMesh(vertices, indices, mesh);

}


void Map::generateQuad() {
	//incarca vertecsii si indecsii din fisier
	std::vector<lab::VertexFormat> vertices;
	std::vector<unsigned int> indices;

	int N = 250;
	vertices.push_back(lab::VertexFormat(-N, 0, -N, 0, 1, 0, 0, 0));
	vertices.push_back(lab::VertexFormat(+N, 0, -N, 0, 1, 0, 1, 0));
	vertices.push_back(lab::VertexFormat(+N, 0, +N, 0, 1, 0, 1, 1));
	vertices.push_back(lab::VertexFormat(-N, 0, +N, 0, 1, 0, 0, 1));

	/*
	indices.push_back(0); indices.push_back(1); indices.push_back(2);
	indices.push_back(2); indices.push_back(3); indices.push_back(0);

	lab::createQuad(vertices[0], vertices[1], vertices[2], vertices[3], mesh);
	//*/

	//*
	indices.push_back(0); indices.push_back(1); indices.push_back(2); indices.push_back(3);

	//vertex array object -> un obiect ce reprezinta un container pentru starea de desenare
	glGenVertexArrays(1, &mesh.vao);
	glBindVertexArray(mesh.vao);

	//vertex buffer object -> un obiect in care tinem vertecsii
	glGenBuffers(1, &mesh.vbo);
	glBindBuffer(GL_ARRAY_BUFFER, mesh.vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(lab::VertexFormat), &vertices[0], GL_STATIC_DRAW);

	//index buffer object -> un obiect in care tinem indecsii
	glGenBuffers(1, &mesh.ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	//ne intereseaza doar pozitiile pt acest lab
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*) 0);								//trimite pozitii pe pipe 0
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*) (sizeof(float) * 3));								//trimite pozitii pe pipe 0
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*) (sizeof(float) * 6));					//trimite texcoords pe pipe1
	mesh.count = indices.size();

	//*/
}

void Map::initPixel() {
	pixel = (unsigned char**) calloc(height, sizeof(unsigned char*));
	for(int i = 0; i < height; i++) {
		pixel[i] = (unsigned char*) calloc(width, sizeof(unsigned char));
	}
}

void Map::destroyGL() {
	glDeleteProgram(shader);
	glDeleteTextures(1, &texture);
}