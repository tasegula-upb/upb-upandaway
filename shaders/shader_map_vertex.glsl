#version 400

layout(location = 0) in vec3 in_position;	
layout(location = 1) in vec3 in_normal;	
layout(location = 2) in vec2 in_texcoord;

uniform mat4 model_matrix;

out vec3 world_pos;
out vec3 world_normal;
out vec2 texcoord;

void main() {
	world_pos		= (model_matrix * vec4(in_position, 1.0)).xyz;
	world_normal	= (model_matrix * vec4(in_normal, 0.0)).xyz;
	texcoord		= in_texcoord;

	gl_Position = model_matrix * vec4(in_position,1); 
}
