#version 400

layout(vertices = 4) out;
uniform int tess_factor_inner;
uniform int tess_factor_outer;

in vec2 v_texcoord[];
in vec3 v_normal[];

out vec3 tc_normal[];
out vec2 tc_texcoord[];

void main(){


	tc_texcoord[gl_InvocationID] = v_texcoord[gl_InvocationID];
	tc_normal[gl_InvocationID] = v_normal[gl_InvocationID];

	//pentru triunghiuri doar primele 3 nivele outer sunt folosite
	gl_TessLevelOuter[0] = tess_factor_outer;
	gl_TessLevelOuter[1] = tess_factor_outer;
	gl_TessLevelOuter[2] = tess_factor_outer;
	gl_TessLevelOuter[3] = tess_factor_outer;
	
	//pentru triunghiuri doar primul nivel interior e folosit
	gl_TessLevelInner[0] = tess_factor_inner;
	gl_TessLevelInner[1] = tess_factor_inner;

	//executat o data per patch
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}
