#version 400
layout(location = 0) out vec4 out_color;

uniform sampler2D map_color;

in vec2 color;
in vec2 texcoord;

void main(){

	out_color = texture(map_color, texcoord);
}