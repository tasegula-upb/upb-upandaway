#version 430

uniform mat4 model_matrix;

out vec3 speed;

struct particle{
	vec4 position;
	vec4 speed;
	vec4 iposition;
	vec4 ispeed;
};

layout(std140,binding=0) buffer particles{
		particle data[];
};

float rand(vec2 co){;
	return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

out vec3 vspeed;

void main(){

	vec3 pos = data[gl_VertexID].position.xyz;
	vec3 spd = data[gl_VertexID].speed.xyz;

	float dt =0.1;

	pos = pos + spd * dt + vec3(0,-0.9,0)*dt*dt/2.0f ;
	spd = spd + vec3(0,-0.9,0)*dt;
	if(pos.y < (-40 + rand(pos.xy)*20)){
		pos = data[gl_VertexID].iposition.xyz;
		spd = data[gl_VertexID].ispeed.xyz;
	}

	vspeed = spd;
	data[gl_VertexID].position.xyz =  pos;
	data[gl_VertexID].speed.xyz =  spd;
	
	gl_Position = model_matrix*vec4(pos,1); 
}
