#version 400

//layout(triangles, equal_spacing, ccw) in;
//layout(triangles, fractional_even_spacing, ccw) in;
layout(quads, fractional_odd_spacing, ccw) in;

uniform sampler2D map_displacement;
uniform float max_displacement;

uniform mat4 view_matrix, projection_matrix;
in vec3 tc_normal[];
in vec2 tc_texcoord[];

out vec2 color;
out vec2 texcoord;

void main(){

	vec3 pozitie = (gl_TessCoord.x * gl_in[0].gl_Position.xyz + (1-gl_TessCoord.x)*gl_in[1].gl_Position.xyz) * gl_TessCoord.y + 
					(gl_TessCoord.x * gl_in[3].gl_Position.xyz + (1-gl_TessCoord.x)*gl_in[2].gl_Position.xyz) * 
					(1-gl_TessCoord.y);

	vec3 normala = normalize((gl_TessCoord.x * tc_normal[0] + (1-gl_TessCoord.x)*tc_normal[1])*gl_TessCoord.y + (gl_TessCoord.x * tc_normal[3] + (1-gl_TessCoord.x)*tc_normal[2])*(1-gl_TessCoord.y));
	texcoord = (gl_TessCoord.x * tc_texcoord[0] + (1-gl_TessCoord.x)*tc_texcoord[1])*gl_TessCoord.y + (gl_TessCoord.x * tc_texcoord[3] + (1-gl_TessCoord.x)*tc_texcoord[2])*(1-gl_TessCoord.y);

	//inaltimea sa 
	pozitie = pozitie + vec3(0,texture(map_displacement,texcoord).x * max_displacement,0)*normala;

	color = gl_TessCoord.xy;
	//color = normala;
	gl_Position = projection_matrix * view_matrix*vec4(pozitie,1);
}
