#version 400
layout(location = 0) out vec4 out_color;

uniform sampler2D map_color;

// material properties
uniform vec3 light_position;
uniform vec3 eye_position;
uniform int material_shininess;
uniform float material_kd;
uniform float material_ks;

uniform sampler2D textura_color;
uniform int use_texture;	// use color or texture

in vec3 WorldPos_FS_in;
in vec3 Normal_FS_in;
in vec2 TexCoord_FS_in;

in vec3 color;
in vec2 texcoord;

void main() {
	//*
	vec3 color;

	if (use_texture == 1) {
		if (WorldPos_FS_in.y == 1) {
			color.r = 0;
			color.g = 1;
			color.b = 0;
		}else{
			color = texture(textura_color, TexCoord_FS_in).xyz;
		}
	}
	else {
		color.r = WorldPos_FS_in.y * 10 / 255;
		color.g = 1 - WorldPos_FS_in.y * 10 /255;
		color.b = 0;
	}

	vec3 l = normalize(light_position - WorldPos_FS_in);
	vec3 v = normalize(eye_position - WorldPos_FS_in);
	vec3 h = normalize(l + v);

	float ambientala = 0.2f;
	float difuza	 = material_kd * max(dot(Normal_FS_in, l), 0);
	float spectrala  = material_ks * pow(max(dot(Normal_FS_in, h), 0), material_shininess) * (difuza < 0 ? 0 : 1);
	
	float light = 0;
	light = ambientala + difuza + spectrala;

	out_color = vec4(difuza * color + spectrala + ambientala, 1);
	//*/

	//out_color = texture(map_color, TexCoord_FS_in);
}