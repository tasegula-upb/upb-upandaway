#version 400

// define the number of CPs in the output patch
layout (vertices = 3) out;

uniform vec3 eye_position;
uniform int tess_factor_inner;
uniform int tess_factor_outer;
uniform int manual_tes;

// attributes of the input CPs
in vec3 world_pos[];
in vec3 world_normal[];
in vec2 texcoord[];

// attributes of the output CPs
out vec3 WorldPos_ES_in[];
out vec3 Normal_ES_in[];
out vec2 TexCoord_ES_in[];

float GetTessLevel(float Distance0, float Distance1)
{
    float AvgDistance = (Distance0 + Distance1) / 2.0;

    if (AvgDistance <= 25.0) {
        return 16.0;
    }
    else if (AvgDistance <= 50.0) {
        return 8.0;
    }
    else {
        return 2.0;
    }
}

void main()
{
    TexCoord_ES_in[gl_InvocationID] = texcoord[gl_InvocationID];
    Normal_ES_in[gl_InvocationID]	= world_normal[gl_InvocationID];
    WorldPos_ES_in[gl_InvocationID] = gl_in[gl_InvocationID].gl_Position.xyz;

	if (manual_tes == 0) {
		float EyeToVertexDistance0 = distance(eye_position, world_pos[0]);
		float EyeToVertexDistance1 = distance(eye_position, world_pos[1]);
		float EyeToVertexDistance2 = distance(eye_position, world_pos[2]);

		gl_TessLevelOuter[0] = GetTessLevel(EyeToVertexDistance1, EyeToVertexDistance2);
		gl_TessLevelOuter[1] = GetTessLevel(EyeToVertexDistance2, EyeToVertexDistance0);
		gl_TessLevelOuter[2] = GetTessLevel(EyeToVertexDistance0, EyeToVertexDistance1);
		gl_TessLevelInner[0] = gl_TessLevelOuter[2] * 2;
	} else {
		gl_TessLevelOuter[0] = tess_factor_outer;
		gl_TessLevelOuter[1] = tess_factor_outer;
		gl_TessLevelOuter[2] = tess_factor_outer;
	
		//pentru triunghiuri doar primul nivel interior e folosit
		gl_TessLevelInner[0] = tess_factor_inner;
	}

	//executat o data per patch
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}