#version 430
layout(points) in;
layout(triangle_strip, max_vertices = 6) out;

uniform mat4  view_matrix, projection_matrix; 

out vec2 g_texcoord;
void main(){
	
	float ds = 0.25 ;

//create particle	
	vec4 p1 = (gl_in[0].gl_Position);
	vec4 p2 = vec4(gl_in[0].gl_Position.x + 1, gl_in[0].gl_Position.y,     gl_in[0].gl_Position.z, 1);
	vec4 p3 = vec4(gl_in[0].gl_Position.x,     gl_in[0].gl_Position.y + 1, gl_in[0].gl_Position.z, 1);
	vec4 p4 = vec4(gl_in[0].gl_Position.x + 1, gl_in[0].gl_Position.y + 1, gl_in[0].gl_Position.z, 1);

	g_texcoord = vec2(0, 0);
	gl_Position = projection_matrix * view_matrix * p1;
	EmitVertex();
	g_texcoord = vec2(1, 0);
	gl_Position = projection_matrix * view_matrix * p2;
	EmitVertex();
	g_texcoord = vec2(1, 1);
	gl_Position = projection_matrix * view_matrix * p4;
	EmitVertex();
	EndPrimitive();
	
	g_texcoord = vec2(0, 0);
	gl_Position = projection_matrix * view_matrix * p1;
	EmitVertex();
	g_texcoord = vec2(0, 1);
	gl_Position = projection_matrix * view_matrix * p3;
	EmitVertex();
	g_texcoord = vec2(1, 1);
	gl_Position = projection_matrix * view_matrix * p4;
	EmitVertex();
	EndPrimitive();
}
